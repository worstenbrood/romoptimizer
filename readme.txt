ROMOptimizer v1.0
-----------------

Romoptimizer will repack all apk/jar/zip files with higher compression and optimize all png/gif,
also inside apk/jar/zip files.

Usage:
------

Put your (preferably deodexed) rom in the "rom" folder, then run romopimizer.bat.
