@ECHO OFF

SET COMPRESSION_LEVEL=9
SET OPTIPNG_LEVEL=7
SET TOOLS_DIR=tools
SET ROM_DIR=rom

ECHO ROMOptimizer v1.0 (worstenbrood@gmail.com)
ECHO ------------------------------------------

ECHO [+] Repacking/optimizing JAR ...
FOR /F "tokens=*" %%F IN ('DIR /B /S "%~dp0%ROM_DIR%\*.JAR"') DO CALL REPACK.BAT %%F

ECHO [+] Repacking/optimizing APK ...
FOR /F "tokens=*" %%F IN ('DIR /B /S "%~dp0%ROM_DIR%\*.APK"') DO CALL REPACK.BAT %%F

ECHO [+] Repacking/optimizing ZIP ...
FOR /F "tokens=*" %%F IN ('DIR /B /S "%~dp0%ROM_DIR%\*.ZIP"') DO CALL REPACK.BAT %%F

ECHO [+] Optimizing PNG ...
FOR /F "tokens=*" %%F IN ('DIR /B /S "%~dp0%ROM_DIR%\*.PNG"') DO CALL OPTIPNG.BAT %%F

ECHO [+] Optimizing GIF ...
FOR /F "tokens=*" %%F IN ('DIR /B /S "%~dp0%ROM_DIR%\*.GIF"') DO CALL OPTIPNG.BAT %%F

SET COMPRESSION_LEVEL=
SET OPTIPNG_LEVEL=
SET TOOLS_DIR=
SET ROM_DIR=