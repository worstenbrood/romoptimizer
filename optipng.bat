@ECHO OFF
IF "%1"=="" GOTO END
ECHO [+] Optipng %~n1%~x1 ..
"%~dp0%TOOLS_DIR%\optipng.exe" -o%OPTIPNG_LEVEL% -quiet "%1"
IF NOT "%ERRORLEVEL%"=="0" GOTO ERROR
ECHO [+] Succesfully optipnged %~n1%~x1 !
GOTO END
:ERROR:
ECHO [%TIME:~0,2%:%TIME:~3,2% %DATE:~3,2%/%DATE:~6,2%/%DATE:~9,4%] Optipng Failed: %1 >> "%~dp0error.log"
ECHO [-] Error during optipnging %~n1%~x1 !
:END